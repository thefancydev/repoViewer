var gulp = require('gulp');
var express = require('express');

gulp.task('serve', ['build', 'watch'], function(){
	var app = express();
	app.use(express.static('public'));
	app.listen(4000, '0.0.0.0');
})