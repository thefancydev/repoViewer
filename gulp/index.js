'use strict';

var fs = require('fs'),
	onlyScripts = require('./util/scriptFilter'),
	tasks = fs.readdirSync('./gulp/tasks/').filter(onlyScripts),
	gulp = require('gulp'),
	del = require('del'),
	express = require('express');

tasks.forEach(function (task) {
	require('./tasks/' + task);
});
