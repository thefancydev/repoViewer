[![build status](https://gitlab.com/roconnor/repoViewer/badges/master/build.svg)](https://gitlab.com/roconnor/repoViewer/commits/master)

<h2>What is it?</h2>
This simple application makes use of the GitLab API to display a circle with elements representing each of my public repositories. This idea was originally from [here](http://ozh.github.io), but I have modified to suit my purposes.

<h2>How does it work?</h2>
This site uses the GitLab oauth2 endpoint to validate and display each of your repositories. This can be helpful for a quick view of all your repos